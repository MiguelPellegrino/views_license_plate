import cv2
import numpy as np

# para cargar una imagen
# image = cv2.imread('cv.png')
# cv2.imshow('Titurlo del frame', image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# para cargar un video
# video = cv2.VideoCapture(0)

# salida = cv2.VideoWriter('salida.avi', cv2.VideoWriter_fourcc(*'XVID'),20.0,(640,480))
# while(video.isOpened()):
#     respueta,imagen = video.read()
#     if respueta == True:
#         cv2.imshow('video', imagen)
#         salida.write(imagen)
#         if cv2.waitKey(1) & 0xFF == ord('s'):
#             break
#     else: break

# video.release()
# salida.release()
# cv2.destroyAllWindows()



# Trabajar con grises en una imagen
# image = cv2.imread('caja_2.jpg',0)
# print("aun no ha pasado el proceso")
# _,trans_1 = cv2.threshold(image,100,255,cv2.THRESH_BINARY)
# print("ya pasado el proceso")
# _,trans_2 = cv2.threshold(image,100,255,cv2.THRESH_BINARY_INV)
# cv2.imshow('Imagen sin evaluar', image)
# cv2.imshow('Objeto de importancia', np.hstack([trans_1,trans_2]))

# cv2.waitKey(0)
# cv2.destroyAllWindows()



#Trabajar con deteccion de color
# video = cv2.VideoCapture('video_personas.mp4')

# red_bajo_1 = np.array([0,100,20],np.uint8)
# red_alto_1 = np.array([8,255,255],np.uint8)

# red_bajo_2 = np.array([175,100,20],np.uint8)
# red_alto_2 = np.array([179,255,255],np.uint8)

# salida = cv2.VideoWriter('salida.avi', cv2.VideoWriter_fourcc(*'XVID'),20.0,(640,480))
# while(video.isOpened()):
#     respueta,imagen = video.read()
#     if respueta == True:
#         hsv = cv2.cvtColor(imagen,cv2.COLOR_BGR2HLS)
#         nuevo_1 = cv2.inRange(hsv,red_bajo_1,red_alto_1)
#         nuevo_2 = cv2.inRange(hsv,red_bajo_2,red_alto_2)
#         make = cv2.add(nuevo_1,nuevo_2)
#         cv2.imshow('video', make)
#         cv2.imshow('video_2', imagen)
#         salida.write(imagen)
#         if cv2.waitKey(2) & 0xFF == ord('s'):
#             break
#     else: break

# video.release()
# salida.release()
# cv2.destroyAllWindows()