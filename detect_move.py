import cv2
import numpy as np
import imutils

video = cv2.VideoCapture('Traffic.mp4')
fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))

while(video.isOpened()):
    respueta,imagen = video.read()
    imagen = imutils.resize(imagen, width=640)
    if respueta == True:

        gray = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)

        cv2.rectangle(imagen,(0,0),(imagen.shape[1],40),(0,0,0),-1)
        texto_estado = "No se ha detectado movimiento"
        color = (80, 255, 0)
        
        #puntos                X   y      x   Y           X                      Y
        area_pts = np.array([[270,50], [400,50], [640,imagen.shape[0]], [20,imagen.shape[0]]])
        
        cv2.drawContours(imagen, [area_pts], -1, color, 2)
        

        imAux = np.zeros(shape=(imagen.shape[:2]), dtype=np.uint8)
        imAux = cv2.drawContours(imAux, [area_pts], -1, (255), -1)
        imAux = cv2.bitwise_and(gray, gray, mask=imAux)

        area = fgbg.apply(imAux)
        area = cv2.morphologyEx(area, cv2.MORPH_OPEN, kernel)
        area = cv2.morphologyEx(area, cv2.MORPH_CLOSE, kernel)
        area = cv2.dilate(area, None, iterations=2)

        cnts = cv2.findContours(area, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
        for cnt in cnts:
            if cv2.contourArea(cnt) > 700:
                x, y, w, h = cv2.boundingRect(cnt)
                cv2.rectangle(imagen, (x,y), (x+w, y+h),(0,255,0), 2)
                texto_estado = "Si se ha detectado movimiento"
                color = (0, 0, 255)

        cv2.putText(imagen, texto_estado, (5,30), cv2.FONT_HERSHEY_SIMPLEX,1,color,2)
        cv2.imshow('area', area)
        cv2.imshow('video', imagen)
        if cv2.waitKey(10) & 0xFF == ord('s'):
            break
    else: break

video.release()
cv2.destroyAllWindows()