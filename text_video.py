import cv2
import numpy as np
import imutils

video = cv2.VideoCapture(0)
textos = []
# salida = cv2.VideoWriter('salida.avi', cv2.VideoWriter_fourcc(*'XVID'),20.0,(640,480))
while(video.isOpened()):
    respueta,imagen = video.read()
    if respueta == True:
        gray = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
        gray = cv2.blur(gray,(1,1))
        canny = cv2.Canny(gray,150,200)
        # canny = cv2.dilate(canny,None,iterations=1)

        cnts,_ = cv2.findContours(canny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        for c in cnts:
            area = cv2.contourArea(c)
            if area > 300:
                cv2.drawContours(imagen,cnts,-1,(255,0,0),2)


        imagen = imutils.resize(imagen, width=720)
        canny = imutils.resize(canny, width=720)
        # cv2.imshow('gray', canny)
        cv2.imshow('video', imagen)
        # salida.write(imagen)
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break
    else: break

video.release()
# salida.release()
cv2.destroyAllWindows()